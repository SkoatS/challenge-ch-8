import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import NavBar  from "./components/routes/navbar";
import HomePage from "./components/register";
import EditPage from "./components/edit-page";
import SearchPage from "./components/search-page";
import LoginPage from "./components/routes/login";
import './App.css';

function App() {
  return (
    <Router>
    <div>
    <NavBar/>
        <Routes>
          <Route path='/' element={<HomePage title="Home Page"/>}> </Route>
          <Route path='/login' element={<LoginPage title="Login Page"/>}> </Route>
          <Route path='/edit-page' element={<EditPage title="Edit Page"/>}> </Route>
          <Route path='/search-page' element={<SearchPage title="Search Page"/>}> </Route>
          <Route path="*" element={<h1>Page not found</h1>}/>
        </Routes>
    </div>
  </Router>
  );
}

export default App;