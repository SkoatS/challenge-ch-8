import { Container, Form, Button } from "react-bootstrap";
import { useState } from "react";
import axios from "axios";
import { logout } from "./routes/logout";

const EditPage = () => {

  const [ usernameBarus, setUsernameBaru] = useState('');
  const [ experiences, setExperience ] = useState('');
  const [ levels, setLevel ] = useState('');

  const updateUser = () => {
    const requestingData = {
      nip: localStorage.getItem("Username"),
      usernameBarus: usernameBarus,
      experience: experiences,
      level: levels
    }
    axios({
      method: "PUT",
      url: "http://localhost:3200/players",
      data: requestingData
    }).then(() => {
      alert("anda akan keluar dari sistem, silahkan login kembali.")
      logout()
    })
  }

  return (
    <Container>
      <Form>
        <Form.Group className="mb-3">
          <Form.Label>Masukan username</Form.Label>
          <Form.Control 
            type="text"
            name="username" 
            className="form-control" 
            onChange={(event) => setUsernameBaru(event.target.value)} 
            defaultValue={localStorage.getItem("username")} />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>experience</Form.Label>
          <Form.Control 
          type="number" 
          name="experience" 
          className="form-control" 
          onChange={(event) => setExperience(event.target.value)}/>
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>level</Form.Label>
          <Form.Control 
          type="number" 
          name="level" 
          className="form-control" 
          onChange={(event) => setLevel(event.target.value)}/>
        </Form.Group>
        <Button variant="primary" type="submit" onChange={(event) => updateUser(event.target.value)} onClick={() => logout()} >
          Edit
        </Button>
      </Form>
    </Container>
  );
};

export default EditPage;
