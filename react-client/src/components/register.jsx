import { Form, Button, FormControl, Container } from "react-bootstrap";
import axios from "axios";
import { useState } from "react";

function HomePage({title}) {

  // Backend
  
  const [ usernames, setUsername] = useState('');
  const [ passwords, setPassword ] = useState('');
  const [ emails, setEmail ] = useState('');

  const handleUsername = (inputUsername) => {
    setUsername(inputUsername)
  }
  
  const handleEmail = (inputEmail) => {
    setEmail(inputEmail)
  }
  const handlePassword = (inputPassword) => {
    setPassword(inputPassword)
  }

  const handleSignUpTask = () => {
    console.log('user register ready')

    const requestingData = {
      username: usernames,
      email: emails, 
      password: passwords
    }
    axios({
      method: "POST",
      url: "http://localhost:5000/players",
      data: requestingData
    }).then((result) => {
      console.log(result.data)
      if (result.data.registered) {
        alert("Register berhasil!")
        window.location.replace("/login")
      } else {
        alert("Gagal mendaftar, coba dengan username lain")
      }
    }).catch((e) => alert('error')) 
}


  return(

    // Frontend

    <Container >
    <Form style={{ marginTop: -100, width: "100%" }}>
    <h3 text={title}>Sign Up</h3>
    <div className="mb-3">
      <Form.Label>Username</Form.Label>
      <FormControl
        type="text"
        className="form-control"
        required onChange={(event) => handleUsername(event.target.value)}
        placeholder="Username"
      />
    </div>
    <div className="mb-3">
      <Form.Label>Email address</Form.Label>
      <FormControl
        type="email"
        className="form-control"
        required onChange={(event) => handleEmail(event.target.value)}
        placeholder="Enter email"
      />
    </div>
    <div className="mb-3">
      <Form.Label>Password</Form.Label>
      <FormControl
        type="password"
        className="form-control"
        required onChange={(event) => handlePassword(event.target.value)}
        placeholder="********"
      />
    </div>
    <div className="d-grid">
      <Button type="submit" className="btn btn-primary" onClick={() => {handleSignUpTask()}}>
        Sign Up
      </Button>
    </div>

  </Form>
  </Container>
  )
}

export default HomePage;