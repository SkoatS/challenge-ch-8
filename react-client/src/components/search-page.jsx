import { Container, Form, Table, Button, FormControl } from "react-bootstrap";
import axios from "axios";
import { useEffect, useState } from "react"

function SearchPage () {
  const [playerList, setPlayerLIst] = useState([])

  useEffect(() => {
    if (!localStorage.getItem("nama") && !localStorage.getItem('username')) {
    console.log(playerList)
      window.location.replace("/")
    }
    axios({
      method: "GET",
      url: "http://localhost:5000/"
    }).then((result)=> setPlayerLIst(result.data))
  })

  return (
    <div>
      <Container className="mt-3">
        <Form>
          <Form.Group className="mb-3">
            <Form.Label>Username</Form.Label>
            <Form.Control type="text" name="name" className="form-control" />
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label>Filter</Form.Label>
            <Form.Select aria-label="Default select example">
              <option>Select filter</option>
              <option value="1">One</option>
              <option value="2">Two</option>
            </Form.Select>
          </Form.Group>
          <Form.Group className="mb-3">
            <Button variant="primary" type="submit">
              Submit
              <FormControl
                  type="email"
                  className="form-control"
                  placeholder="Enter email"
                />
            </Button>
          </Form.Group>
        </Form>
      </Container>

      <Container className="mt-3">
        <h1>Player Data</h1>;
        <Table striped bordered hover variant="dark">
          <thead>
            <tr>
              <th>Nama</th>
              <th>Username{localStorage.getItem("username")}</th>
              <th>Email{localStorage.getItem("email")}</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Bege</td>
              <td>PahlawanSejati</td>
              <td>begePahlawanSejati@gmail.com</td>
            </tr>
          </tbody>
        </Table>
      </Container>
    </div>
  );
};

export default SearchPage;
