import { Form, Button, FormControl, Container } from "react-bootstrap";
import axios from "axios";
import { useState } from "react";

function LoginPage({title}) {

  // Backend
  
  const [ usernames, setUsername] = useState('')
  const [ passwords, setPassword ] = useState('');
  const [ emails, setEmail ] = useState('');

  const handleUsername = (inputUsername) => {
    setUsername(inputUsername)
  }
  
  const handleEmail = (inputEmail) => {
    setEmail(inputEmail)
  }
  const handlePassword = (inputPassword) => {
    setPassword(inputPassword)
  }

  const handleLoginTask = () => {
    console.log('user login ready!')
 

    const requestingData = {
      nip: usernames,
      email: emails,
      password: passwords
    }
    axios({
      method: "POST",
      url: "http://localhost:5000/players/login",
      data: requestingData
    }).then((result) => {
      localStorage.setItem("username", result.data.users.username)
      localStorage.setItem("e-mail", result.data.users.email)
      window.location.replace("/search-page")
    })
  }


  return(

    // Frontend

    <Container >
    <Form style={{ marginTop: -100, width: "100%" }}>
    <h3 text={title}>Login Page</h3>
    <div className="mb-3">
      <Form.Label>Username</Form.Label>
      <FormControl
        type="text"
        className="form-control"
        onChange={(event) => handleUsername(event.target.value)}
        placeholder="Username"
      />
    </div>
    <div className="mb-3">
      <Form.Label>Email address</Form.Label>
      <FormControl
        type="email"
        className="form-control"
        onChange={(event) => handleEmail(event.target.value)}
        placeholder="Enter email"
      />
    </div>
    <div className="mb-3">
      <Form.Label>Password</Form.Label>
      <FormControl
        type="password"
        className="form-control"
        onChange={(event) => handlePassword(event.target.value)}
        placeholder="********"
      />
    </div>
    <div className="d-grid">
      <Button type="submit" className="btn btn-primary" onClick={() => {handleLoginTask()}}>
        Sign Up
      </Button>
    </div>

  </Form>
  </Container>
  )
}

export default LoginPage;