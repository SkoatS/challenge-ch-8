import { Container, Navbar, Nav } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

const NavBar = () => {
  return (
    <Navbar bg="dark" variant="dark">
      <Container>
        <Navbar.Brand>Player Information</Navbar.Brand>
        <Nav className="me-auto">
          <LinkContainer to="/">
            <Nav.Link> Home </Nav.Link>
          </LinkContainer>
          <LinkContainer to="/edit-page">
            <Nav.Link>Edit Player</Nav.Link>
          </LinkContainer>
          <LinkContainer to="/search-page">
            <Nav.Link>Search Player</Nav.Link>
          </LinkContainer>
        </Nav>
      </Container>
    </Navbar>
  );
};

export default NavBar;

